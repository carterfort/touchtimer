//
//  SettingsTableViewController.swift
//  TouchTimer
//
//  Created by Carter Fort on 10/24/15.
//  Copyright © 2015 Out to Lunch Productions. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    @IBOutlet var fastTimerSwitch: UISwitch!
    @IBOutlet var useHealthKit: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fastTimerSwitch.setOn(NSUserDefaults.standardUserDefaults().boolForKey("Settings::FastTimer"), animated: false)
        self.useHealthKit.setOn(NSUserDefaults.standardUserDefaults().boolForKey("Settings::UseHealthKit"), animated: false)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func updateFastTimer(sender: UISwitch) {
        NSUserDefaults.standardUserDefaults().setBool(sender.on, forKey: "Settings::FastTimer")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    @IBAction func doneButtonPressed(sender: AnyObject) {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func updateHealthKitToggle(sender: UISwitch) {
        NSUserDefaults.standardUserDefaults().setBool(sender.on, forKey: "Settings::UseHealthKit")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
}
