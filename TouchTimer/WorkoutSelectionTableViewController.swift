//
//  WorkoutSelectionTableViewController.swift
//  TouchTimer
//
//  Created by Carter Fort on 10/10/15.
//  Copyright © 2015 Out to Lunch Productions. All rights reserved.
//

import UIKit

class WorkoutSelectionTableViewController: UITableViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return StoredWorkouts.sharedInstance.workouts.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Workout Cell", forIndexPath: indexPath)

        cell.textLabel!.text = StoredWorkouts.sharedInstance.workouts[indexPath.row].name

        return cell
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "StartWorkout")
        
        {
            let viewController = segue.destinationViewController as! WorkoutViewController
            
            let indexPath = self.tableView.indexPathForCell((sender as! UITableViewCell))
            
            viewController.workout = StoredWorkouts.sharedInstance.workouts[indexPath!.row]
        }

        
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

    @IBAction func newWorkout(sender: AnyObject) {
        
        //Create a new workout
        
    }

}
