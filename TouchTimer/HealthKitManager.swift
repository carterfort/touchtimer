//
//  HealthkitController.swift
//  TouchTimer
//
//  Created by Carter Fort on 10/12/15.
//  Copyright © 2015 Out to Lunch Productions. All rights reserved.
//

import UIKit
import HealthKit

class WorkoutHealthKitListener
{
    
    var healthkitManager : HealthKitManager?

    
    init()
    {
        
        healthkitManager = HealthKitManager.sharedInstance
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "workoutWasConfirmed:", name: "workoutWasConfirmed", object: nil)
        
    }

    
    @objc func workoutWasConfirmed(notification : NSNotification)
    {
        let workout = notification.object as! Workout
        healthkitManager?.completedSecondsForWorkout(workout.secondsOfExercise, calories: workout.caloriesBurned, workout: workout)
    }
}

class HealthKitManager
{
    static let sharedInstance = HealthKitManager( store: HKHealthStore() )
    
    private let store : HKHealthStore
    
    init(store : HKHealthStore)
    {
        self.store = store
    }
    
    private func requestPermission()
    {
        store.requestAuthorizationToShareTypes([
                HKWorkoutType.workoutType(),
                HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)!
            ],
            readTypes: [
                HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!
            ])
            { (success, error) -> Void in
                
        }
    }
    
    func completedSecondsForWorkout( seconds : Int, calories : Double, workout : Workout)
    {
        
        requestPermission()
        
        print(workout.name)
        print("Seconds : \(seconds)")
        print("Calories : \(calories)")
        
        let caloriesQuantity = HKQuantity(unit: HKUnit.kilocalorieUnit(), doubleValue: calories)
        let timeAgo = NSTimeInterval(seconds * -1)
        let startDate = NSDate().dateByAddingTimeInterval(timeAgo)
        
        let sessionWorkout = HKWorkout(
                                        activityType: .Boxing,
                                        startDate: startDate,
                                        endDate: NSDate(),
                                        duration: NSTimeInterval(seconds),
                                        totalEnergyBurned: caloriesQuantity,
                                        totalDistance: nil,
                                        metadata: [
                                            "Round Timer Workout" : workout.name
                                        ]
        )
        
        let energyBurnedType = HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)
        let energyBurnedForWorkout = HKQuantitySample(type: energyBurnedType!, quantity: caloriesQuantity,
            startDate: startDate, endDate: NSDate())
        
        
        if (NSUserDefaults.standardUserDefaults().boolForKey("Settings::UseHealthKit")){
            
            store.saveObject(sessionWorkout) { (success, error) -> Void in
                
                
                self.store.addSamples([energyBurnedForWorkout], toWorkout: sessionWorkout, completion: { (success, error) -> Void in
                    if ((error) != nil)
                    {
                        print("Error in attaching samples to workout")
                        print(error)
                    }
                })
                
                if ((error) != nil)
                {
                    print("Error in saving to HealthKit")
                    print(error)
                }
            }
            

            
        } else {
            print("Not saving to HealthKit because of the setting")
        }

        
        
    }
    
    private func secondsToCalories(seconds : Int) -> Double {
    
        return 50;
    
    }
    
    
}
