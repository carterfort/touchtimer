//
//  WorkoutTimer.swift
//  TouchTimer
//
//  Created by Carter Fort on 10/11/15.
//  Copyright © 2015 Out to Lunch Productions. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox

class WorkoutTimer
{
    private let delegate : WorkoutTimerDelegate
    private let workout : Workout
    
    private var secondsRemaining : Int = 0
    private var roundsRemaining : Int = 0
    private var timerIsRunning : Bool = false
    private var state : TimerState = .WarmingUp
    private var sounds : [String: AVAudioPlayer]
    private let appDelegate : AppDelegate
    private var secondsOfExercise : Int = 0

    private var secondsElapsed : Int = 0

    private var timer : NSTimer?
    
    init(workout : Workout, delegate : WorkoutTimerDelegate)
    {
        self.delegate = delegate
        self.workout = workout
        self.sounds = [:]
        self.appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        initSounds()
        setup()
        
    }
    
    func isRunning() -> Bool
    {
        return timerIsRunning
    }
    
    private func initSounds()
    {
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            
            let bellPath = NSBundle.mainBundle().pathForResource("bell", ofType: "wav")
            do {
                
                let bell = try AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: bellPath!))
                bell.prepareToPlay()
                self.sounds["bell"] = bell
                
            } catch {
                print("Missing file at path \(bellPath) for bell sound")
            }
            
            let hornPath = NSBundle.mainBundle().pathForResource("horn", ofType: "wav")
            do {
                
                let horn = try AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: hornPath!))
                horn.prepareToPlay()
                self.sounds["horn"] = horn
                
            } catch {
                print("Missing file at path \(hornPath) for horn sound")
            }
        }
    }
    
    private func changeState(newState : TimerState)
    {
        self.state = newState
        self.delegate.timerStateDidChange(newState)
    }
    
    private func setup()
    {
        
        state = .WarmingUp
        secondsRemaining = workout.warmupSeconds
        roundsRemaining = workout.rounds
        secondsElapsed = 0
        delegate.timerStateDidChange(.WarmingUp)
        delegate.timerDidUpdate(self, timerOutput: ConvertSecondsToTime.displayTime(secondsRemaining), roundOutput: displayRoundCount(), workoutOutput: displayWorkoutOutput())

    }
    
    @objc private func start()
    {
        var timerSpeed : Double = 1
        if (NSUserDefaults.standardUserDefaults().boolForKey("Settings::FastTimer")){
            timerSpeed = 0.01
        }
        
        timer = NSTimer.scheduledTimerWithTimeInterval(timerSpeed, target: self, selector: "update", userInfo: nil, repeats: true)
        
        timer?.fire()
        timerIsRunning = true
    }
    
    func stop()
    {
        timer?.invalidate()
        timerIsRunning = false
        delegate.timerDidUpdate(self, timerOutput: ConvertSecondsToTime.displayTime(secondsRemaining), roundOutput: displayRoundCount(), workoutOutput: displayWorkoutOutput())
    }
    
    func go()
    {
        switch(timerIsRunning)
        {
        case true:
            stop()
            break
        case false:
            start()
            break
        }
    }
    
    func reset()
    {
        stop()
        setup()
    }
    
    
    @objc private func update(){
        
        secondsRemaining--
        secondsElapsed++
        
        switch(state)
        {
            case .Active:
                workout.secondsOfExercise++
            break
            
            default:
            break
        }
        
        if (secondsRemaining == 0)
        {
            //Start the next round
            timerReachedZero()
        }
        
        delegate.timerDidUpdate(self, timerOutput: ConvertSecondsToTime.displayTime(secondsRemaining), roundOutput: displayRoundCount(), workoutOutput: displayWorkoutOutput())

    }
    
    private func timerReachedZero()
    {
        
        switch(state)
        {
        case .WarmingUp:
            secondsRemaining = workout.secondsPerRound
            changeState(.Active)
            playSound("bell")
            break
        case .Active:
            
            NSNotificationCenter.defaultCenter().postNotificationName("roundDidEnd", object: workout)
            
            secondsRemaining = workout.secondsPerRest
            changeState(.Resting)
            playSound("horn")
            
            break
        case .Resting:
            
            roundsRemaining--
            secondsRemaining = workout.secondsPerRound
            
            if (roundsRemaining == 0)
            {
                secondsRemaining = workout.cooldownSeconds
                changeState(.CoolingDown)
                playSound("horn")
            } else {
                changeState(.Active)
                playSound("bell")

            }
            
            break
        case .CoolingDown:
            
            endWorkout()
            
            break
            
        }
        
    }
    
    private func endWorkout()
    {
                
        changeState(.WarmingUp)
        delegate.workoutDidFinish(workout)
        reset()
        
    }
    
    
    private func displayRoundCount() -> String
    {
        
        var displayText = ""
        
        switch(state)
            
        {
        case .WarmingUp:
            displayText = "Warming up"
            break
        case .CoolingDown:
            displayText = "Cooling down"
            break
        case .Active, .Resting:
            displayText = "Round \(workout.rounds - roundsRemaining + 1)/\(workout.rounds)"
            break
        }
        
        return displayText
    }
    
    private func displayWorkoutOutput() -> String
    {
        return "\(workout.name) : \(ConvertSecondsToTime.displayTime(workout.totalTime - secondsElapsed))"
    }
    
    
    private func playSound( name : String)
    {
        
        sounds[name]!.play()
        
    }
    
}

class ConvertSecondsToTime
{
    static func displayTime(forSeconds : Int) -> String
    {
        let (hours, minutes, seconds) = (forSeconds / 3600, (forSeconds % 3600) / 60, (forSeconds % 3600) % 60)
        
        var displayTime = ""
        
        if (hours > 0)
        {
            displayTime += String(format: "%02d", hours) + ":"
        }
        
        displayTime += String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds)
        
        return displayTime
    }
}

struct Sound
{
    let name : String
    let systemSound : SystemSoundID
}

enum TimerState
{
    case WarmingUp, Resting, Active, CoolingDown
}

protocol WorkoutTimerDelegate
{
    func timerStateDidChange(state : TimerState)
    func timerDidUpdate(timer : WorkoutTimer, timerOutput : String, roundOutput : String, workoutOutput : String )
    func workoutDidFinish( workout : Workout)
    
}