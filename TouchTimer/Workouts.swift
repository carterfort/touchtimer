//
//  Workouts.swift
//  TouchTimer
//
//  Created by Carter Fort on 10/10/15.
//  Copyright © 2015 Out to Lunch Productions. All rights reserved.
//

import Foundation
import UIKit

class Workout : NSObject
{
    let name : String
    let rounds : Int
    let secondsPerRound : Int
    let secondsPerRest : Int
    let warmupSeconds : Int
    let cooldownSeconds : Int
    let identifier : String
    let icon : UIApplicationShortcutIcon
    let caloriesBurnedPerSecond : Double
    
    var totalTime : Int {
        get{
            return ((secondsPerRound + secondsPerRest) * rounds) + warmupSeconds + cooldownSeconds
        }
    }
    
    var secondsOfExercise : Int = 0
    var caloriesBurned : Double {
        get {
            return Double(Double(secondsOfExercise) * caloriesBurnedPerSecond)
        }
    }
    
    init(name: String, rounds : Int, secondsPerRound : Int, secondsPerRest: Int, warmupSeconds : Int, cooldownSeconds : Int, identifier : String, icon : UIApplicationShortcutIcon, caloriesBurnedPerSecond : Double) {
        
        self.name = name;
        self.rounds = rounds
        self.secondsPerRound = secondsPerRound
        self.secondsPerRest = secondsPerRest
        self.warmupSeconds = warmupSeconds
        self.cooldownSeconds = cooldownSeconds
        self.identifier = identifier
        self.icon = icon
        self.caloriesBurnedPerSecond = caloriesBurnedPerSecond
        
        super.init()
        
    }
}

class StoredWorkouts {
    
    static let sharedInstance = StoredWorkouts()
    
    var workouts : [Workout] {
        
        get {
            return [
                Workout(
                    name: "Bag Work",
                    rounds : 5,
                    secondsPerRound : 120,
                    secondsPerRest: 60,
                    warmupSeconds : 10,
                    cooldownSeconds : 60,
                    identifier : ShortcutIdentifier.First.type,
                    icon : UIApplicationShortcutIcon(type: UIApplicationShortcutIconType.Play),
                    caloriesBurnedPerSecond : 0.15
                ),
                Workout(
                    name: "Pad Work",
                    rounds : 10,
                    secondsPerRound : 180,
                    secondsPerRest: 60,
                    warmupSeconds : 10,
                    cooldownSeconds : 60,
                    identifier : ShortcutIdentifier.Second.type,
                    icon : UIApplicationShortcutIcon(type: UIApplicationShortcutIconType.Play),
                    caloriesBurnedPerSecond : 0.15
                ),
                Workout(
                    name: "Shadow Boxing",
                    rounds : 10,
                    secondsPerRound : 60,
                    secondsPerRest: 30,
                    warmupSeconds : 10,
                    cooldownSeconds : 60,
                    identifier : ShortcutIdentifier.Third.type,
                    icon : UIApplicationShortcutIcon(type: UIApplicationShortcutIconType.Play),
                    caloriesBurnedPerSecond : 0.15
                )
            ]

        }
    }
    
    
    
}