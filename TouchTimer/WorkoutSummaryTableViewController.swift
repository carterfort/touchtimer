//
//  WorkoutSummaryTableViewController.swift
//  TouchTimer
//
//  Created by Carter Fort on 10/18/15.
//  Copyright © 2015 Out to Lunch Productions. All rights reserved.
//

import UIKit

protocol WorkoutSummaryDelegate
{
    func workoutWasInvalidated(workout : Workout)
    func userMarkedWorkoutAsValid(workout : Workout)
}

class WorkoutSummaryTableViewController: UITableViewController {

    var workout : Workout?
    var delegate : WorkoutSummaryDelegate?
    
    @IBOutlet var totalTimeLabel: UILabel!
    @IBOutlet var caloriesBurnedLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.totalTimeLabel.text = ConvertSecondsToTime.displayTime(self.workout!.secondsOfExercise)
        self.caloriesBurnedLabel.text = "\(self.workout!.caloriesBurned)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func completeWorkout(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) { () -> Void in
            self.delegate?.userMarkedWorkoutAsValid(self.workout!)
        }
    }

    @IBAction func cancelWorkout(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) { () -> Void in
            self.delegate?.workoutWasInvalidated(self.workout!)
        }
        
    }
    @IBAction func resumeWorkoutButtonPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
