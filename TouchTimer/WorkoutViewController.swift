//
//  WorkoutViewController.swift
//  TouchTimer
//
//  Created by Carter Fort on 10/10/15.
//  Copyright © 2015 Out to Lunch Productions. All rights reserved.
//

import UIKit

class WorkoutViewController: UIViewController, WorkoutTimerDelegate, WorkoutSummaryDelegate {
    
    var workout : Workout?
    
    private var workoutTimer : WorkoutTimer?

    @IBOutlet weak var roundCountLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet var workoutNameLabel: UILabel!
    @IBOutlet var startStopButton: UIButton!
    @IBOutlet var finishButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UIApplication.sharedApplication().idleTimerDisabled = true
        workoutTimer = WorkoutTimer(workout: workout!, delegate: self)
        self.title = self.workout!.name
        self.workoutNameLabel.text = "\(self.workout!.name) : \(ConvertSecondsToTime.displayTime(self.workout!.totalTime))"
        
        self.startStopButton.setTitle("Start", forState: .Normal)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.sharedApplication().idleTimerDisabled = false
    }
    
    @IBAction func finishWorkoutButtonPressed(sender: AnyObject) {
        if(workout!.secondsOfExercise == 0)
        {
            workoutWasInvalidated(workout!)
        } else {
            workoutDidFinish(workout!)
        }
    }
    
    @IBAction func resetButtonPressed(sender: AnyObject) {
        workoutTimer!.reset()
    }

    @IBAction func startStopButtonPressed(sender: AnyObject) {
        workoutTimer!.go()
    }
    
    func timerStateDidChange(state : TimerState)
    {
        
        var newBackgroundColor : UIColor
        
        switch(state)
        {
        case .Active:

            newBackgroundColor = UIColor(red: 183/255.0, green: 31/255.0, blue: 40/255.0, alpha: 1)
            break
        case .CoolingDown:
            
            newBackgroundColor = UIColor(red: 95/255.0, green: 182/255.0, blue: 246/255.0, alpha: 1)
            
            break
        case .Resting:

            newBackgroundColor = UIColor(red: 54/255.0, green: 195/255.0, blue: 73/255.0, alpha: 1)
            
            break
        case .WarmingUp:
            
            newBackgroundColor = UIColor(red: 0.96, green:0.69, blue:0.06, alpha: 1)
            
            break
        }
        
        view.backgroundColor = newBackgroundColor
        
    }
    
    func timerDidUpdate(timer: WorkoutTimer, timerOutput : String, roundOutput : String, workoutOutput : String )
    {
        timerLabel.text = timerOutput
        roundCountLabel.text = roundOutput
        self.workoutNameLabel.text = workoutOutput
        if (timer.isRunning())
        {
            self.startStopButton.setTitle("Pause", forState: UIControlState.Normal)
        } else {
            self.startStopButton.setTitle("Resume", forState: .Normal)
        }
    }
    
    func workoutDidFinish( workout : Workout)
    {
        displayCompleteWorkoutMessage()
    }
    
    
    func displayCompleteWorkoutMessage()
    {
        let workoutSummaryVC : WorkoutSummaryTableViewController = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()).instantiateViewControllerWithIdentifier("Workout Summary") as! WorkoutSummaryTableViewController
        workoutSummaryVC.workout = workout
        workoutSummaryVC.delegate = self;
        
        self.presentViewController(workoutSummaryVC, animated: true, completion: nil)

    }

    func workoutWasInvalidated(workout : Workout)
    {
        self.dismissViewControllerAnimated(true, completion: nil)

    }
    
    func userMarkedWorkoutAsValid(workout : Workout)
    {
        NSNotificationCenter.defaultCenter().postNotificationName("workoutWasConfirmed", object: workout )
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        workoutTimer!.stop()
    }
    
    override func willMoveToParentViewController(parent: UIViewController?) {
        //super.willMoveToParentViewController(parent)
        if parent == nil {
            print("This VC is 'will' be popped. i.e. the back button was pressed.")
        }
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
